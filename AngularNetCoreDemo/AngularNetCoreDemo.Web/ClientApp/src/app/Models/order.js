"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Order = /** @class */ (function () {
    function Order(ClientName, City, Address, Phone, Count) {
        this.ClientName = ClientName;
        this.City = City;
        this.Address = Address;
        this.Phone = Phone;
        this.Count = Count;
    }
    return Order;
}());
exports.Order = Order;
//# sourceMappingURL=order.js.map