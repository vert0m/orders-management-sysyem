import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../orders.service';
import { OrderModel } from '../Models/orderModel';
import { PagedData } from '../Models/pagedData';
import { Order } from '../Models/order';
import { convertEnumToString } from '../Models/orderState';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  private orders: OrderModel[];
  private chosenOrder: OrderModel;
  private createdOrder: OrderModel;
  private allUniqCities: string[] = [];
  private idForDelete: string;

  private arrayOfPages: number[];
  private currentPage: number;
  private totalNumberOfPages: number;

  private shouldShowDeleteModal = false;

  private httpError;

  private convertEnumToString = convertEnumToString;

  private filterClientName = '';
  private filterDate = '';
  private filterCount = 0;
  private filterCity = 'All cities';

  constructor(private orderService: OrdersService) { }

  ngOnInit() {
    this.getOrders(1);
  }

  getOrders(page: number, filterClientName: string = '',
    filterDate: string = '', filterCity: string = '', filterCount: number = 0) {
    this.orderService.getPagedOrders(page, filterClientName, filterCity, filterCount, filterDate, '')
      .subscribe((pagedData: PagedData<OrderModel>) => {
        this.orders = pagedData.results;
        this.arrayOfPages = Array.from(Array(pagedData.totalNumberOfPages).keys());
        this.currentPage = page;
        this.totalNumberOfPages = pagedData.totalNumberOfPages;

        if (filterClientName === '' && filterDate === '' && filterCity === '' && filterCount === 0) {
          this.allUniqCities = [];
          this.allUniqCities.push('All cities');
          const allCities: string[] = [];
          pagedData.results.map(order => allCities.push(order.city));
          allCities.filter((x, i, a) => a.indexOf(x) === i).map(city => this.allUniqCities.push(city));
        }
      }, error => {
        this.httpError = error;
        console.log(error);
      });
  }

  chooseCity(city: string) {
    this.filterCity = city;
    this.filter();
  }

  filter(): void {
    if (this.filterCount == null) {
      this.filterCount = 0;
    }

    if (this.filterCity === this.allUniqCities[0]) {
      this.getOrders(1, this.filterClientName, this.filterDate, '', this.filterCount);
      return;
    }

    this.getOrders(1, this.filterClientName, this.filterDate, this.filterCity, this.filterCount);
  }

  editOrder(order: OrderModel): void {
    this.chosenOrder = order;
  }

  cancel(): void {
    this.chosenOrder = null;
  }

  cancelModal(): void {
    this.shouldShowDeleteModal = false;
  }

  deleteModal(): void {
    this.orderService.deleteOrder(this.idForDelete).subscribe(
      () => this.getOrders(this.currentPage),
      error => {
        this.httpError = error;
        console.log(error);
      });

    this.shouldShowDeleteModal = false;
  }

  deleteOrder(id: string): void {
    this.idForDelete = id;
    this.shouldShowDeleteModal = true;
  }

  save(): void {
    if (this.chosenOrder.id == null) {
      this.orderService.createOrder(new Order(this.chosenOrder.clientName, this.chosenOrder.city,
        this.chosenOrder.address, this.chosenOrder.phone, this.chosenOrder.count))
        .subscribe(
          (order: OrderModel) => this.createdOrder = order,
        error => {
          this.httpError = error;
          console.log(error);
        });
    } else {
      this.orderService.updateOrder(this.chosenOrder.id, new Order(this.chosenOrder.clientName,
        this.chosenOrder.city, this.chosenOrder.address, this.chosenOrder.phone, this.chosenOrder.count))
        .subscribe(
        (order: OrderModel) => this.createdOrder = order,
        error => { this.httpError = error; console.log(error); }
      );
    }

    this.cancel();
  }

  reloadPage = () => location.reload();

  showPrevPage(): void {
    if (this.currentPage === 1) {
      return;
    }

    this.getOrders(this.currentPage - 1);
  }

  showNextPage(): void {
    if (this.currentPage === this.totalNumberOfPages) {
      return;
    }

    this.getOrders(this.currentPage + 1);
  }
}
