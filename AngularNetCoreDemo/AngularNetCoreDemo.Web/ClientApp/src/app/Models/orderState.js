"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OrderState;
(function (OrderState) {
    OrderState[OrderState["Created"] = 0] = "Created";
    OrderState[OrderState["Processing"] = 1] = "Processing";
    OrderState[OrderState["Sent"] = 2] = "Sent";
    OrderState[OrderState["Delivered"] = 3] = "Delivered";
})(OrderState = exports.OrderState || (exports.OrderState = {}));
function convertEnumToString(state) {
    switch (state) {
        case OrderState.Created: return 'Created';
        case OrderState.Delivered: return 'Delivered';
        case OrderState.Processing: return 'Processing';
        case OrderState.Sent: return 'Sent';
    }
}
exports.convertEnumToString = convertEnumToString;
//# sourceMappingURL=orderState.js.map