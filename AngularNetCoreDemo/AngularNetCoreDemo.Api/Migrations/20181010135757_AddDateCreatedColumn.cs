﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AngularNetCoreDemo.Api.Migrations
{
    public partial class AddDateCreatedColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "Orders",
                nullable: true);

            migrationBuilder.Sql(@"UPDATE Orders SET DateCreated = '2018-10-01' WHERE DateCreated IS NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "Orders");
        }
    }
}
