import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Order } from './Models/order';
import { OrderModel } from './Models/OrderModel';
import { environment  } from '../environments/environment';
import { Observable } from 'rxjs/Observable';
import { PagedData } from './Models/pagedData';
import { ReportModel } from './Models/reportModel';

@Injectable()
export class OrdersService {
  constructor(private http: HttpClient) { }

  createOrder(order: Order): Observable<OrderModel> {
    return this.http.post<OrderModel>(environment.baseUrl + 'orders', order);
  }

  updateOrder(id: string, order: Order): Observable<OrderModel> {
    return this.http.put<OrderModel>(environment.baseUrl + `orders/${id}`, order);
  }

  deleteOrder(id: string) {
    return this.http.delete(environment.baseUrl + `orders/${id}`);
  }

  getReport(dateFrom?: string, dateTo?: string): Observable<ReportModel> {
    return this.http.get<ReportModel>(environment.baseUrl + `orders/report?dateFrom=${dateFrom}&dateTo=${dateTo}`);
  }

  getPagedOrders(page: number, clientName?: string, city?: string ,
    count?: number, dateFrom?: string, dateTo?: string, pageSize: number = 10): Observable<PagedData<OrderModel>> {
    return this.http.get<PagedData<OrderModel>>(environment.baseUrl +
      `orders/paged?page=${page}&pageSize=${pageSize}&clientName=${clientName}&city=${city}&dateFrom=${dateFrom}&dateTo=${dateTo}&count=${count}`);
  }
}
