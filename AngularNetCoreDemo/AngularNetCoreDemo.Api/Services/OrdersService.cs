﻿using AngularNetCoreDemo.Api.Enities;
using AngularNetCoreDemo.Api.Models;
using AngularNetCoreDemo.Api.Repositories;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AngularNetCoreDemo.Api.Services
{
    public interface IOrderService
    {
        Task<PagedResults<Order>> GetPagedOrders(CancellationToken cancellationToken, int page, int pageSize, string clientName, string city,
            int? count, DateTime? dateFrom, DateTime? dateTo);
        Task<ReportModel> GetReport(CancellationToken cancellationToken, DateTime? dateFrom, DateTime? dateTo);
    }

    public class OrdersService : IOrderService
    {
        private readonly IOrdersRepository _ordersRepository;

        public OrdersService(IOrdersRepository ordersRepository)
        {
            _ordersRepository = ordersRepository;
        }

        public async Task<ReportModel> GetReport(CancellationToken cancellationToken, DateTime? dateFrom, DateTime? dateTo)
        {
            var orders = await _ordersRepository.Get(cancellationToken);

            var cityAmountList = orders
                .Where(x => (dateFrom == null || x.DateCreated >= dateFrom) &&
                    (dateTo == null || x.DateCreated <= dateTo))
                .GroupBy(x => x.City)
                .Select(x => new CityAmountModel { City = x.Key, AmountByCity = x.Sum(c => c.Count) })
                .ToList();

            var totalAmount = cityAmountList.Sum(x => x.AmountByCity);

            return new ReportModel
            {
                CityAmountList = cityAmountList,
                TotalAmount = totalAmount
            };
        }

        public async Task<PagedResults<Order>> GetPagedOrders(CancellationToken cancellationToken, int page, int pageSize, string clientName, string city, 
            int? count, DateTime? dateFrom, DateTime? dateTo)
        {
            var orders = await _ordersRepository.Get(cancellationToken);

            var skipAmount = pageSize * (page - 1);

            var query = orders
                .Skip(skipAmount)
                .Take(pageSize)
                .Where(x => 
                (clientName == null || x.ClientName == clientName) &&
                (city == null || x.City == city) &&
                (count == null || x.Count >= count) &&
                (dateFrom == null || x.DateCreated >= dateFrom) &&
                (dateTo == null || x.DateCreated <= dateTo));

            int totalNumberOfRecords;

            if (clientName != null || city != null || count != 0 || dateFrom != null || dateTo != null)
            {
                totalNumberOfRecords = query.Count();
            }
            else
            {
                totalNumberOfRecords = orders.Count();
            }

            var mod = totalNumberOfRecords % pageSize;

            var totalPageCount = (totalNumberOfRecords / pageSize) + (mod == 0 ? 0 : 1);

            return new PagedResults<Order>
            {
                Results = query,
                PageNumber = page,
                PageSize = query.Count(),
                TotalNumberOfPages = totalPageCount,
                TotalNumberOfRecords = totalNumberOfRecords
            };
        }
    }
}
