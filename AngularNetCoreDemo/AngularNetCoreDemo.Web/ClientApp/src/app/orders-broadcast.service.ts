import { Injectable } from '@angular/core';
import signalR = require('@aspnet/signalr');
import { HubConnection } from '@aspnet/signalr';
import { environment } from '../environments/environment';

@Injectable()
export class OrdersBroadcastService {
  public connection: HubConnection;

  constructor() {
  }

  startConnection() {
    this.connection = new signalR.HubConnectionBuilder()
      .withUrl(environment.ordersBroadcasrUrl)
      .build();

    return this.connection.start();
  }

  sendMessage(orderId: string) {
    this.connection.send('Broadcast', orderId);
  }

  closeConnection() {
    this.connection.stop();
  }
}
