export class CityAmountModel {
  constructor(
    public city: string,
    public amountByCity: number) { }
}

export class ReportModel {
  constructor(
    public cityAmountList: CityAmountModel[],
    public totalAmount: number) { }
}
