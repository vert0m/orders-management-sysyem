﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace AngularNetCoreDemo.Api
{
    public class ConfigHelper
    {
        private readonly ConfigurationBuilder _configurationBuilder;
        private readonly IConfigurationRoot _configurationRoot;

        private const string CONFIG_FILE_NAME = "appsettings.json";

        public ConfigHelper()
        {
            _configurationBuilder = new ConfigurationBuilder();

            _configurationBuilder.SetBasePath(Directory.GetCurrentDirectory());
            _configurationBuilder.AddJsonFile(CONFIG_FILE_NAME);

            _configurationRoot = _configurationBuilder.Build();
        }

        public string GetDefaultConnectionString() =>
            _configurationRoot.GetConnectionString("DefaultConnection");

        public string GetConnectionStringByName(string connectionStringName) =>
            _configurationRoot.GetConnectionString(connectionStringName);

        public string GetValueFromConstsSection(string sectionName) =>
            _configurationRoot.GetSection("Consts").GetSection(sectionName).Get<string>();
    }
}
