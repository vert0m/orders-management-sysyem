﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AngularNetCoreDemo.Api.Enities;
using AngularNetCoreDemo.Api.Models;
using AngularNetCoreDemo.Api.Repositories;
using AngularNetCoreDemo.Api.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace AngularNetCoreDemo.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrdersRepository _ordersRepository;
        private readonly IOrderService _orderService;

        public OrdersController(IOrdersRepository ordersRepository, IOrderService orderService)
        {
            _ordersRepository = ordersRepository;
            _orderService = orderService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Order>>> Get(CancellationToken cancellationToken)
        {
            var response = await _ordersRepository.Get(cancellationToken);

            return response == null ? 
                (ActionResult<IEnumerable<Order>>)NotFound() : 
                (ActionResult<IEnumerable<Order>>)Ok(response);
        }

        [HttpGet]
        [Route("paged")]
        public async Task<ActionResult<PagedResults<Order>>> Get(CancellationToken cancellationToken, [FromQuery] int page, [FromQuery] int pageSize = 10,
            [FromQuery] string clientName = null, [FromQuery] string city = null, [FromQuery] int? count = null, [FromQuery] DateTime? dateFrom = null, [FromQuery] DateTime? dateTo = null)
        {
            var response = await _orderService.GetPagedOrders(cancellationToken, page, pageSize, clientName, city, count, dateFrom, dateTo);

            return response == null ?
                (ActionResult<PagedResults<Order>>)NotFound() :
                (ActionResult<PagedResults<Order>>)Ok(response);
        }

        [HttpGet]
        [Route("report")]
        public async Task<ActionResult<ReportModel>> GetReport(CancellationToken cancellationToken, [FromQuery] DateTime? dateFrom = null, [FromQuery] DateTime? dateTo = null)
        {
            var response = await _orderService.GetReport(cancellationToken, dateFrom, dateTo);

            return response == null ?
                (ActionResult<ReportModel>)NotFound() :
                (ActionResult<ReportModel>)Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Order>> GetById(Guid id, CancellationToken cancellationToken)
        {
            var response = await _ordersRepository.GetById(id, cancellationToken);

            return response == null ?
                (ActionResult<Order>)NotFound() :
                (ActionResult<Order>)Ok(response);
        }

        [HttpPost]
        public async Task<ActionResult<Order>> Create([FromBody] Order order, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var response = await _ordersRepository.Create(order, cancellationToken);

            return Ok(response);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(Guid id, [FromBody] Order order, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var response = await _ordersRepository.Update(id, order, cancellationToken);

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task Delete(Guid id, CancellationToken cancellationToken)
        {
            await _ordersRepository.Delete(id, cancellationToken);
        }
    }
}
