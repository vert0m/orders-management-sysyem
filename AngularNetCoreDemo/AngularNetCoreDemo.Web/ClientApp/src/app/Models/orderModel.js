"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OrderModel = /** @class */ (function () {
    function OrderModel(id, clientName, city, address, phone, count, dateCreated, state) {
        this.id = id;
        this.clientName = clientName;
        this.city = city;
        this.address = address;
        this.phone = phone;
        this.count = count;
        this.dateCreated = dateCreated;
        this.state = state;
    }
    return OrderModel;
}());
exports.OrderModel = OrderModel;
//# sourceMappingURL=orderModel.js.map