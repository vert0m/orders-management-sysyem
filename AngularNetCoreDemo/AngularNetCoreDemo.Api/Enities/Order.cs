﻿using AngularNetCoreDemo.Api.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace AngularNetCoreDemo.Api.Enities
{
    public class Order
    {
        public Order()
        {
            DateCreated = DateTime.Now;
            State = OrderState.Created;
        }

        public Guid Id { get; set; }
        [Required]
        public string ClientName { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        [Phone]
        public string Phone { get; set; }
        [Required]
        public int Count { get; set; }
        public DateTime? DateCreated { get; set; }
        public OrderState State { get; set; }
    }
}
