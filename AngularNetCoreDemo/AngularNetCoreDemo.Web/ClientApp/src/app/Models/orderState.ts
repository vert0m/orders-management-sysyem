export enum OrderState {
  Created,
  Processing,
  Sent,
  Delivered
}

export function convertEnumToString(state: OrderState): string {
  switch (state) {
    case OrderState.Created: return 'Created';
    case OrderState.Delivered: return 'Delivered';
    case OrderState.Processing: return 'Processing';
    case OrderState.Sent: return 'Sent';
  }
}
