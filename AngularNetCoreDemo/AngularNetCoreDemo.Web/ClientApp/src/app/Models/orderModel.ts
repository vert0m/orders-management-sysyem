import { OrderState } from "./orderState";

export class OrderModel {
  constructor(public id: string,
    public clientName: string,
    public city: string,
    public address: string,
    public phone: string,
    public count: number,
    public dateCreated: string,
    public state: OrderState) { }
}
