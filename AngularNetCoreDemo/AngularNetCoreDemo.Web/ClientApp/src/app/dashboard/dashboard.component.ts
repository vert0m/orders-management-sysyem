import { Component, OnInit, OnDestroy } from '@angular/core';
import { OrderModel } from '../Models/OrderModel';
import { convertEnumToString } from '../Models/orderState';
import { OrdersBroadcastService } from '../orders-broadcast.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  private orders: OrderModel[] = [];

  private httpError;

  private convertEnumToString = convertEnumToString;

  constructor(private orderBroadcastService: OrdersBroadcastService) {
  }

  ngOnInit() {
    this.orderBroadcastService
      .startConnection()
      .catch(err => this.httpError = err);

    this.orderBroadcastService
      .connection
      .on('Broadcast', orders => this.orders = orders);

    this.orderBroadcastService
      .connection
      .on('OnConnected', orders => this.orders = orders);
  }

  ngOnDestroy() {
    this.orderBroadcastService.closeConnection();
  }

  reloadPage = () => location.reload();
}
