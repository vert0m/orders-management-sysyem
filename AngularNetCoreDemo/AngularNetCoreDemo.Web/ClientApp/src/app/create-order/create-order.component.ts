import { Component, OnInit, OnDestroy } from '@angular/core';
import { OrdersService } from '../orders.service';
import { Order } from '../Models/order';
import { OrderModel } from '../Models/OrderModel';
import { NgForm } from '@angular/forms';
import { OrdersBroadcastService } from '../orders-broadcast.service';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.css']
})
export class CreateOrderComponent implements OnInit, OnDestroy {
  private shouldShowForm = true;
  private shouldShowAlert = false;

  private createdOrder: OrderModel;
  private httpError;

  constructor(private ordersService: OrdersService, private ordersBroadcastService: OrdersBroadcastService) {

  }

  ngOnInit() {
    this.ordersBroadcastService
      .startConnection()
      .catch(err => this.httpError = err);
  }

  ngOnDestroy() {
    this.ordersBroadcastService.closeConnection();
  }

  addOrder(clientName: string, city: string, address: string, phone: string, count: number, createOrderForm: NgForm): void {
    if (clientName == null || city == null || address == null || phone == null || count == null) {
      this.shouldShowAlert = true;
      return;
    }

    this.ordersService.createOrder(new Order(clientName, city, address, phone, count))
      .subscribe(
      (order: OrderModel) => {
        this.createdOrder = order;
        this.ordersBroadcastService.sendMessage(order.id);
      },
      error => {
        this.httpError = error;
        console.log(error);
      });

    this.shouldShowForm = false;

    createOrderForm.reset();
  }

  showForm(): void {
    this.shouldShowForm = true;
  }
}
