"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CityAmountModel = /** @class */ (function () {
    function CityAmountModel(city, amountByCity) {
        this.city = city;
        this.amountByCity = amountByCity;
    }
    return CityAmountModel;
}());
exports.CityAmountModel = CityAmountModel;
var ReportModel = /** @class */ (function () {
    function ReportModel(cityAmountList, totalAmount) {
        this.cityAmountList = cityAmountList;
        this.totalAmount = totalAmount;
    }
    return ReportModel;
}());
exports.ReportModel = ReportModel;
//# sourceMappingURL=reportModel.js.map