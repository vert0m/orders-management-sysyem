export class PagedData<T> {
  constructor(
    public pageNumber: number,
    public pageSize: number,
    public totalNumberOfPages: number,
    public totalNumberOfRecords: number,
    public results: T[]) { }
}
