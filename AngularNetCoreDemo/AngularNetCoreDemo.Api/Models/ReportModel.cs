﻿using System.Collections.Generic;

namespace AngularNetCoreDemo.Api.Models
{
    public class CityAmountModel
    {
        public string City { get; set; }
        public int AmountByCity { get; set; }
    }

    public class ReportModel
    {
        public List<CityAmountModel> CityAmountList { get; set; }
        public int TotalAmount { get; set; }
    }
}
