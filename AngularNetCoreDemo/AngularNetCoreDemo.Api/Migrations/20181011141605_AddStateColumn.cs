﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AngularNetCoreDemo.Api.Migrations
{
    public partial class AddStateColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "State",
                table: "Orders",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.Sql(@"UPDATE Orders SET State = 3");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "State",
                table: "Orders");
        }
    }
}
