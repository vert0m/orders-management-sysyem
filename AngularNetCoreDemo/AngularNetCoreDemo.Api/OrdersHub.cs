﻿using AngularNetCoreDemo.Api.Repositories;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AngularNetCoreDemo.Api
{
    public class OrdersHub : Hub
    {
        public Task Broadcast(Guid orderId)
        {
            var repository = new OrdersRepository();

            while (true)
            {
                if (repository.GetById(orderId, CancellationToken.None).Result == null)
                {
                    continue;
                }
                else
                {
                    var top10orders = repository.Get(CancellationToken.None)
                        .Result
                        .OrderByDescending(x => x.DateCreated)
                        .Take(10);

                    return Clients
                        .All
                        .SendAsync("Broadcast", top10orders);
                }              
            }
        }

        public override async Task OnConnectedAsync()
        {
            var repository = new OrdersRepository();
            var top10orders = repository.Get(CancellationToken.None)
                .Result
                .OrderByDescending(x => x.DateCreated)
                .Take(10);

            await Clients.Caller.SendAsync("OnConnected", top10orders);
            await base.OnConnectedAsync();
        }
    }
}
