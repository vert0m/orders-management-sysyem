﻿using AngularNetCoreDemo.Api.Enities;
using Microsoft.EntityFrameworkCore;

namespace AngularNetCoreDemo.Api
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }

        private readonly ConfigHelper _configHelper;

        public ApplicationContext()
        {
            _configHelper = new ConfigHelper();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configHelper.GetDefaultConnectionString());
        }
    }
}
