using AngularNetCoreDemo.Api.Controllers;
using AngularNetCoreDemo.Api.Enities;
using AngularNetCoreDemo.Api.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AngularNetCoreDemo.Tests
{
    [TestClass]
    public class OrdersControllerTest
    {
        private OrdersRepository _repository;
        private OrdersController _controller;

        private readonly Order testOrder = new Order
        {
            ClientName = "testName",
            City = "testCity",
            Address = "testAddress",
            Count = 0,
            Phone = "000"
        };

        [TestInitialize]
        public void SetupContext()
        {
            _repository = new OrdersRepository();
            _controller = new OrdersController(_repository, null);
        }

        [TestMethod]
        public async Task GetTest()
        {
            var cancellationToken = new CancellationToken();

            var orders = await _controller.Get(cancellationToken) as ActionResult<IEnumerable<Order>>;

            var ordersFromRepository = await _repository.Get(cancellationToken);

            Assert.IsNotNull(orders);
            Assert.AreEqual(ordersFromRepository.Count(), ((orders.Result as OkObjectResult).Value as IEnumerable<Order>).Count());
        }

        [TestMethod]
        public async Task GetByIdTest()
        {
            var cancellationToken = new CancellationToken();

            testOrder.Id = Guid.NewGuid();

            await _repository.Create(testOrder, cancellationToken);

            var order = await _controller.GetById(testOrder.Id, cancellationToken) as ActionResult<Order>;

            var orderFromRepository = await _repository.GetById(testOrder.Id, cancellationToken);

            Assert.IsNotNull(order);

            Assert.AreEqual(((order.Result as OkObjectResult).Value as Order).ClientName, orderFromRepository.ClientName);
            Assert.AreEqual(((order.Result as OkObjectResult).Value as Order).City, orderFromRepository.City);
            Assert.AreEqual(((order.Result as OkObjectResult).Value as Order).Phone, orderFromRepository.Phone);
            Assert.AreEqual(((order.Result as OkObjectResult).Value as Order).Address, orderFromRepository.Address);

            await _repository.Delete(testOrder.Id, cancellationToken);
        }

        [TestMethod]
        public async Task CreateTest()
        {
            var cancellationToken = new CancellationToken();

            testOrder.Id = Guid.NewGuid();

            var createdOrder = await _controller.Create(testOrder, cancellationToken) as ActionResult<Order>; ;

            var orders = await _repository.Get(cancellationToken);

            Assert.IsNotNull(orders);
            Assert.IsTrue(orders.Any(x => 
                x.ClientName == testOrder.ClientName &&
                x.City == testOrder.City &&
                x.Address == testOrder.Address &&
                x.Count == testOrder.Count &&
                x.Phone == testOrder.Phone
            ));

            Assert.AreEqual(((createdOrder.Result as OkObjectResult).Value as Order).DateCreated, orders.FirstOrDefault(x => x.Id == testOrder.Id).DateCreated);

            await _repository.Delete(testOrder.Id, cancellationToken);
        }

        [TestMethod]
        public async Task UpdateTest()
        {
            var cancellationToken = new CancellationToken();

            testOrder.Id = Guid.NewGuid();

            await _controller.Create(testOrder, cancellationToken);

            testOrder.City = "111";

            await _controller.Update(testOrder.Id, testOrder, cancellationToken);

            var orderFromRepository = await _repository.GetById(testOrder.Id, cancellationToken);

            Assert.IsNotNull(orderFromRepository);
            Assert.AreEqual(testOrder.City, orderFromRepository.City);

            await _repository.Delete(testOrder.Id, cancellationToken);
        }

        [TestMethod]
        public async Task DeleteTest()
        {
            var cancellationToken = new CancellationToken();

            testOrder.Id = Guid.NewGuid();

            var createdOrder = await _repository.Create(testOrder, cancellationToken);

            await _controller.Delete(testOrder.Id, cancellationToken);

            var orders = await _repository.Get(cancellationToken);

            Assert.IsFalse(orders.Any(x =>
                x.ClientName == testOrder.ClientName &&
                x.City == testOrder.City &&
                x.Address == testOrder.Address &&
                x.Count == testOrder.Count &&
                x.Phone == testOrder.Phone &&
                x.DateCreated == createdOrder.DateCreated
            ));
        }
    }
}
