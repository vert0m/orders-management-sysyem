"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OrdersPagedData = /** @class */ (function () {
    function OrdersPagedData(PageNumber, PageSize, TotalNumberOfPages, TotalNumberOfRecords, Results) {
        this.PageNumber = PageNumber;
        this.PageSize = PageSize;
        this.TotalNumberOfPages = TotalNumberOfPages;
        this.TotalNumberOfRecords = TotalNumberOfRecords;
        this.Results = Results;
    }
    return OrdersPagedData;
}());
exports.OrdersPagedData = OrdersPagedData;
//# sourceMappingURL=pagedData.js.map