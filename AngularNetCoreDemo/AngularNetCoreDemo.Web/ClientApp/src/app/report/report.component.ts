import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../orders.service';
import { OrderModel } from '../Models/OrderModel';
import { ReportModel, CityAmountModel } from '../Models/reportModel';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  private cityAmountList: CityAmountModel[];
  private totalAmount: number;
  private httpError;

  private filterDateFrom = '';
  private filterDateTo = '';

  constructor(private ordersService: OrdersService) { }

  ngOnInit() {
    this.getOrders();
  }

  getOrders(dateFrom: string = '', dateTo: string = ''): void {
    this.ordersService.getReport(dateFrom, dateTo).subscribe((report: ReportModel) => {
      this.cityAmountList = report.cityAmountList;
      this.totalAmount = report.totalAmount;
    }
      , error => {
      this.httpError = error;
      console.log(error);
    });
  }

  filter(): void {
    this.getOrders(this.filterDateFrom, this.filterDateTo);
  }

  reloadPage = () => location.reload();
}
