﻿using AngularNetCoreDemo.Api.Enities;
using AngularNetCoreDemo.Api.Enums;
using AngularNetCoreDemo.Api.Repositories;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AngularNetCoreDemo.Api
{
    public class StateWorker : IHostedService, IDisposable
    {
        private Timer _timer;
        private ApplicationContext _db;
        private readonly ConfigHelper _configHelper;

        public StateWorker()
        {
            _db = new ApplicationContext();
            _configHelper = new ConfigHelper();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(ChangeOrdersStates, null, TimeSpan.Zero, TimeSpan.FromSeconds(Convert.ToDouble(_configHelper.GetValueFromConstsSection("StateUpdateInterval"))));

            return Task.CompletedTask;
        }

        private void ChangeOrdersStates(object state)
        {
            var orders = _db.Orders.ToList();

            var random = new Random();

            foreach (var order in orders)
            {
                var orderState = random.Next(4);

                order.State = (OrderState)orderState;

                _db.Update(order);
                _db.SaveChanges();
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
            _db.Dispose();
        }
    }
}
