import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateOrderComponent } from './create-order/create-order.component';
import { OrdersComponent } from './orders/orders.component';
import { ReportComponent } from './report/report.component';

import { OrdersService } from './orders.service';
import { OrdersBroadcastService } from './orders-broadcast.service';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    DashboardComponent,
    CreateOrderComponent,
    OrdersComponent,
    ReportComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: DashboardComponent, pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'create', component: CreateOrderComponent },
      { path: 'orders', component: OrdersComponent },
      { path: 'report', component: ReportComponent }
    ])
  ],
  providers: [OrdersService, OrdersBroadcastService],
  bootstrap: [AppComponent]
})
export class AppModule { }
