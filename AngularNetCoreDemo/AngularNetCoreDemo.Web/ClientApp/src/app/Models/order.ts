export class Order {
  constructor(
    public ClientName: string,
    public City: string,
    public Address: string,
    public Phone: string,
    public Count: number) { }
}
