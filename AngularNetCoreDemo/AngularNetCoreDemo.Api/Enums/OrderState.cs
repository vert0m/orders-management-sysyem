﻿namespace AngularNetCoreDemo.Api.Enums
{
    public enum OrderState : byte
    {
        Created,
        Processing,
        Sent,
        Delivered
    }
}
