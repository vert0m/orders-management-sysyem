import { TestBed, inject } from '@angular/core/testing';

import { OrdersBroadcastService } from './orders-broadcast.service';

describe('OrdersBroadcastService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrdersBroadcastService]
    });
  });

  it('should be created', inject([OrdersBroadcastService], (service: OrdersBroadcastService) => {
    expect(service).toBeTruthy();
  }));
});
