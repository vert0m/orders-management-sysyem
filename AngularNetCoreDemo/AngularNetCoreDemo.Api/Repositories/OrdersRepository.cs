﻿using AngularNetCoreDemo.Api.Enities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AngularNetCoreDemo.Api.Repositories
{
    public interface IOrdersRepository
    {
        Task<IEnumerable<Order>> Get(CancellationToken cancellationToken);
        Task<Order> GetById(Guid id, CancellationToken cancellationToken);
        Task<Order> Create(Order item, CancellationToken cancellationToken);
        Task<Order> Update(Guid id, Order item, CancellationToken cancellationToken);
        Task Delete(Guid id, CancellationToken cancellationToken);
    }

    public class OrdersRepository : IOrdersRepository
    {
        private readonly ApplicationContext _db;

        public OrdersRepository()
        {
            _db = new ApplicationContext();
        }

        public async Task<Order> Create(Order order, CancellationToken cancellationToken)
        {
            var newOrder = await _db.Orders.AddAsync(order);
            await _db.SaveChangesAsync(cancellationToken);

            return newOrder.Entity;
        }

        public async Task<Order> Update(Guid id, Order order, CancellationToken cancellationToken)
        {
            var orderFromDb = await _db.Orders.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

            orderFromDb.Address = order.Address;
            orderFromDb.City = order.City;
            orderFromDb.ClientName = order.ClientName;
            orderFromDb.Count = order.Count;
            orderFromDb.Phone = order.Phone;

            var updatedOrder = _db.Orders.Update(orderFromDb);
            await _db.SaveChangesAsync(cancellationToken);

            return updatedOrder.Entity;
        }

        public async Task Delete(Guid id, CancellationToken cancellationToken)
        {
            var order = await _db.Orders.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

            if (order != null)
                _db.Orders.Remove(order);

            await _db.SaveChangesAsync(cancellationToken);
        }

        public async Task<IEnumerable<Order>> Get(CancellationToken cancellationToken)
        {
            return await _db.Orders.ToListAsync();
        }

        public async Task<Order> GetById(Guid id, CancellationToken cancellationToken)
        {
            return await _db.Orders.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }
    }
}
